from napalm import get_network_driver
import json

driver = get_network_driver('ios')
optional_args = {'secret': 'cisco'} #cisco is the enable password
ios = driver('131.226.217.150', 'admin', 'C1sco12345', optional_args=optional_args)
ios.open()

output = ios.get_interfaces()
dump = json.dumps(output, sort_keys=True, indent=4)
print(dump)

# output = ios.get_interfaces_ip()
# dump = json.dumps(output, sort_keys=True, indent=4)
# print(dump)

#end your code
ios.close()
